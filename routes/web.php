<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Post;
use App\User;
use App\Role;
use App\Country;
use App\Photo;
use App\Tag;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//
//Route::get('/about', function () {
//    return "Hi about page";
//});
//
//Route::get('/contact', function () {
//    return "Hi i am contact";
//});
//
//Route::get('/post/{id}/{name}', function ($id, $name) {
//    return "this is post number ". $id. " " . $name;
//});
//
//Route::get('admin/post/example', array('as'=>'admin.home',function (){
//    $url = route('admin.home');
//    return "this url is " . $url;
//}));



//Route::get('/post/{name}', 'PostsController@index');

//Route::resource('posts', 'PostsController');

//Route::get('/contact', 'PostsController@contact');

//Route::get('post/{id}', 'PostsController@show_post');


/*
|--------------------------------------------------------------------------
| DATABASE Raw SQL Queries
|--------------------------------------------------------------------------
|

|
*/


//Route::get('/insert', function (){
//    DB::insert('insert into posts(title,content) value(?, ?)',[' Laravel','Ita text']);
//});

//Route::get('/read/{id}',function ($id){
//    $results = DB::select('select * from posts where id = ?', [$id]);
//
//    foreach ($results as $post){
//        return $post->title;
//    }
//
//});

//Route::get('/update',function (){
//    $updated = DB::update('update posts set title ="update title" where id = ?', [1]);
//    return $updated;
//});

//Route::get('/delete',function (){
//    $deleted = DB::delete('delete from posts where id = ?', [1]);
//    return $deleted;
//});

////////----------------------------------------------------------------------------------------------------------

/*
|--------------------------------------------------------------------------
| ELOQUENT
|--------------------------------------------------------------------------
|
*/

//Route::get('/find',function (){
//
//    $posts = App\Post::all();
//
//    foreach ($posts as $post){
//        return $post->title;
//    }
//});

//Route::get('/findwhere', function (){
//    $posts = Post::where('is_admin',0)->orderBy('id', 'asc')->take(2)->get();
//    return $posts;
//});
//
//Route::get('/findmore', function (){
//    $posts = Post::findOrFail(1);
//    return $posts;
//});


//Route::get('/basicinsert', function (){
//    $post = new Post;
//
//    $post->title = 'new element title insert';
//    $post->content = 'tex tex tex tex text';
//
//    $post->save();
//});
//
//Route::get('/basicinsert', function (){
//
//
//    $post =Post::find(2);
//    $post->title = 'new element title insert';
//    $post->content = 'tex tex tex tex text 2222222222222222';
//
//    $post->save();
//});

Route::get('/create', function (){
    Post::create(['title'=>'create method','content'=>'wow text text text']);
});

//Route::get('/update',function (){
//    Post::where('id', 2)->where('is_admin', 0)->update(['title'=>'update method', 'content'=>'contentttt text text textdfhgfh']);
//});

//Route::get('/delete',function (){
//    $post = Post::find(2);
//    $post->delete();
//});

//Route::get('/delete2',function (){
//    Post::destroy([4,5]);
//});

//Route::get('/softdelete',function (){
// Post::find(6)->delete();
//});

//Route::get('/readsoftdelete', function (){
//    $post = Post::withTrashed()->where('is_admin', 0)->get();
//    return $post;
//});

//Route::get('/readsoftdelete2', function (){
//    $post = Post::onlyTrashed()->where('is_admin', 0)->get();
//    return $post;
//});

//Route::get('/restore', function (){
//    Post::withTrashed()->where('is_admin', 0)->restore();
//});

//Route::get('/forcedelete', function (){
//    Post::find(6)->forceDelete();
//});

////////----------------------------------------------------------------------------------------------------------

/*
|--------------------------------------------------------------------------
| Eloquent Relationship
|--------------------------------------------------------------------------
|
|
*/

//-----------------One to One--------------------------------------------------

//Route::get('/user/{id}/post', function ($id){
//    return User::find($id)->post;
//});
//
//Route::get('/post/{id}/user', function($id){
//return Post::find($id)->user->name;
//});

//-----------------One to many--------------------------------------------------
//Route::get('/posts',function (){
//   $user = User::find(1);
//   foreach ($user->posts as $post){
//       echo $post->title . '<br>';
//    }
//});

//-----------------Many to many--------------------------------------------------

//Route::get('user/{id}/role', function ($id){
//    $user = User::find($id);
//    foreach ($user->roles as $role){
//        return $role->name;
//    }
//});


//-----------------Has many through--------------------------------------------------


//Route::get('/user/country',function (){
//    $country = Country::find(1);
//    foreach ($country->posts as $post){
//        return $post->title;
//    };
//});

//-----------------polymorphic --------------------------------------------------
//Route::get('user/photos',function (){
//    $user = User::find(1);
//    foreach ($user->photos as $photo){
//        return $photo;
//    }
//});
//
//Route::get('photo/{id}/post', function($id){
//      $photo =  Photo::findOrFail($id);
//      return $photo->imageable;
//});

//-----------------polymorphic many to many --------------------------------------------------
//Route::get('/post/tag',function (){
//    $post = Post::find(1);
//    foreach ($post->tags as $tag){
//        echo $tag->name;
//    }
//
//});
//Route::get('/tag/post',function (){
//    $tag = Tag::find(2);
//    foreach ($tag->posts as$post){
//        echo  $post->title;
//    }
//});
//-----------------------------------------------------------------------------



/////--------------------------forms section-----------------------------


Route::resource('/posts', 'PostsController');
